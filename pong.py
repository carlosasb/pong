

import simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True

# helper functions
def spawn_ball(direction):
    """
    Spawns a new ball at the center of the PONG table
    with a velocity vector pointed towards the side 
    of the scoring player
    
    direction => boolean defining the direction 
                 of the ball
    """
    
    # global variables
    global ball_pos, ball_vel
    
    # reset ball position
    ball_pos = [WIDTH / 2, HEIGHT / 2]
    
    # reset ball velocity values
    # |v_x| = 120 / 60 to 239 / 60
    # v_y = -60 / 60 to -180 / 60
    ball_vel = [random.randrange(120, 241) / 60.0, -1 * random.randrange(60, 181) / 60.0]
    
    # define directions for the ball
    if (direction):
        ball_vel[0] = ball_vel[0]
    else:
        ball_vel[0] = -ball_vel[0]
        
def collision():
    """
    Handles scoring events and paddle collision
    """
    
    # global variables
    global ball_vel
    global paddle1_pos, paddle1_vel
    global paddle2_pos, paddle2_vel
    global score1, score2

    # checks ball collision with gutters or paddles
    if (ball_pos[0] <= PAD_WIDTH + BALL_RADIUS):
        if (paddle1_pos[1] - HALF_PAD_HEIGHT <= ball_pos[1] <= paddle1_pos[1] + HALF_PAD_HEIGHT):
            ball_vel[0] += ball_vel[0] * 0.1
            ball_vel[1] += ball_vel[1] * 0.1
            ball_vel[0]  = -ball_vel[0]
        else:
            score2 += 1
            spawn_ball(RIGHT)
            paddle1_pos = [HALF_PAD_WIDTH, HEIGHT / 2]
            paddle2_pos = [WIDTH - HALF_PAD_WIDTH, HEIGHT / 2]
    elif (ball_pos[0] >= WIDTH - BALL_RADIUS - PAD_WIDTH):
        if (paddle2_pos[1] - HALF_PAD_HEIGHT <= ball_pos[1] <= paddle2_pos[1] + HALF_PAD_HEIGHT):
            ball_vel[0] += ball_vel[0] * 0.1
            ball_vel[1] += ball_vel[1] * 0.1
            ball_vel[0]  = -ball_vel[0]
        else:
            score1 += 1
            spawn_ball(LEFT)
            paddle1_pos = [HALF_PAD_WIDTH, HEIGHT / 2]
            paddle2_pos = [WIDTH - HALF_PAD_WIDTH, HEIGHT / 2]

# define event handlers
def new_game():
    """
    Initializes a new game of PONG
    """
    
    # global variables
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are numbers
    global score1, score2  # these are ints
    
    # initializes global variables
    # scores for player 1 and 2
    score1 = 0
    score2 = 0
    
    # paddle 1
    paddle1_pos = [HALF_PAD_WIDTH, HEIGHT / 2]
    paddle1_vel = 0

    # paddle 2
    paddle2_pos = [WIDTH - HALF_PAD_WIDTH, HEIGHT / 2]
    paddle2_vel = 0
    
    # spawns a new ball
    if (random.randrange(0,2) == 0):
        spawn_ball(LEFT)
    else:
         spawn_ball(RIGHT)

def draw(canvas):
    """
    Draws the board, paddles and ball onto the canvas
    
    canvas => object of type canvas
    """
    
    # global variables
    global score1, score2
    global paddle1_pos, paddle2_pos
    global ball_pos, ball_vel
    
    # draw mid line and gutters
    canvas.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    canvas.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    canvas.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
        
    # bounces the ball on the top and botton walls
    if (ball_pos[1] <= BALL_RADIUS or ball_pos[1] >= HEIGHT - BALL_RADIUS):
        ball_vel[1] = - ball_vel[1]

    # update ball
    ball_pos[1] += ball_vel[1]
    ball_pos[0] += ball_vel[0]
            
    # draw ball
    canvas.draw_circle(ball_pos, BALL_RADIUS, 1, "White", "White")

    # paddle1
    # update paddle1's vertical position
    paddle1_pos[1] += paddle1_vel
    
    # set paddle1 position to maximum or minimum possible
    # if on botton or top of the canvas
    if (paddle1_pos[1] <= HALF_PAD_HEIGHT):
        paddle1_pos[1] = HALF_PAD_HEIGHT
    elif (paddle1_pos[1] >= HEIGHT - HALF_PAD_HEIGHT):
        paddle1_pos[1] = HEIGHT - HALF_PAD_HEIGHT

    # paddle2
    # update paddle1's vertical position
    paddle2_pos[1] += paddle2_vel
    
    # set paddle1 position to maximum or minimum possible
    # if on botton or top of the canvas
    if (paddle2_pos[1] <= HALF_PAD_HEIGHT):
        paddle2_pos[1] = HALF_PAD_HEIGHT
    elif (paddle2_pos[1] >= HEIGHT - HALF_PAD_HEIGHT):
        paddle2_pos[1] = HEIGHT - HALF_PAD_HEIGHT
    
    # draw paddles
    canvas.draw_polygon([[paddle1_pos[0] - HALF_PAD_WIDTH, paddle1_pos[1] - HALF_PAD_HEIGHT], 
                        [paddle1_pos[0] + HALF_PAD_WIDTH, paddle1_pos[1] - HALF_PAD_HEIGHT], 
                        [paddle1_pos[0] + HALF_PAD_WIDTH, paddle1_pos[1] + HALF_PAD_HEIGHT], 
                        [paddle1_pos[0] - HALF_PAD_WIDTH, paddle1_pos[1] + HALF_PAD_HEIGHT]], 
                        1, "White", "White")
    canvas.draw_polygon([[paddle2_pos[0] - HALF_PAD_WIDTH, paddle2_pos[1] - HALF_PAD_HEIGHT], 
                        [paddle2_pos[0] + HALF_PAD_WIDTH, paddle2_pos[1] - HALF_PAD_HEIGHT], 
                        [paddle2_pos[0] + HALF_PAD_WIDTH, paddle2_pos[1] + HALF_PAD_HEIGHT], 
                        [paddle2_pos[0] - HALF_PAD_WIDTH, paddle2_pos[1] + HALF_PAD_HEIGHT]], 
                        1, "White", "White")
    
    # determine whether paddle and ball collide
    collision()
    
    # draw scores
    canvas.draw_text(str(score1), [14 * WIDTH / 40, HEIGHT / 4], 50, "White")
    canvas.draw_text(str(score2), [24 * WIDTH / 40, HEIGHT / 4], 50, "White")
    
def keydown(key):
    """
    Handles key press events
    
    key => char containing the code for the pressed key
    """
    
    # global variables
    global paddle1_vel, paddle2_vel
    
    # declare local variable
    # acceleration
    acc = 3
    
    # checks for arrow down and up presses
    # applies acc to paddle1 and paddle2 velocities
    # paddle1
    if (key == simplegui.KEY_MAP["w"]):
        paddle1_vel -= acc
    elif (key==simplegui.KEY_MAP["s"]):
        paddle1_vel += acc
    
    # paddle2
    if(key == simplegui.KEY_MAP["up"]):
        paddle2_vel -= acc
    elif (key==simplegui.KEY_MAP["down"]):
        paddle2_vel += acc
   
def keyup(key):
    """
    Handles key release events
    
    key => char containing the code for the released key
    """
    
    # global variables
    global paddle1_vel, paddle2_vel
    
    # declare local variable
    # acceleration
    acc = -3
    
    # checks for arrow down and up presses
    # applies acc to paddle1 and paddle2 velocities
    # paddle1
    if (key == simplegui.KEY_MAP["w"]):
        paddle1_vel -= acc
    elif (key==simplegui.KEY_MAP["s"]):
        paddle1_vel += acc
    
    # paddle2
    if(key == simplegui.KEY_MAP["up"]):
        paddle2_vel -= acc
    elif (key==simplegui.KEY_MAP["down"]):
        paddle2_vel += acc

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
frame.add_button("Restart", new_game)

# start frame
new_game()
frame.start()
