###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================#

This is a README file for the my Python implementation of the classic arcade
game PONG. Written in Python 2.7.10. 

At the moment, many functionalities of this implementation are tied to 
simpleGUI (see http://www.codeskulptor.org/# for more information). Also it is 
not a class yet, though the structure is very similar to a class.

Basic functionality is there. Two paddles are drawn to the screen and a 
ball is created at the middle of the screen with a random (inside a range) 
velocity vector, directed upwards to either the left or right (also random).

Scoring is similar to the classic game. There is, however, one key difference.
There is a space with the width of the paddles from the left and right edges
of the screen. They are called gutters and serve the purpose of simplifying
the collision code. Note that the paddle strikes only increase the x velocity
component of the ball velocity, while the original game also modified the
vertical velocity as well. 

To use, copy the code to the editor in http://www.codeskulptor.org/# and 
press the run button and enjoy. 

Note for students whose project is implementing this game. Do not copy this
code and use it. It is unethical and your instructor (or whoever is grading
your project) will also be able to find it. If you do it for this purpose, I 
hope you get a zero.

TODO:

Turn it into a class. This is basic.

Remove dependence on simpleGUI. This is more dificult since a large portion of
the functionality present in this code is tied to it. One possible option is
to use ncurses libraries (I don't know if there are Python bindings, so there
is that). 

Add GTK+ compatibility. This will take a while, since I am a veritable 
noob at GTK+. 
